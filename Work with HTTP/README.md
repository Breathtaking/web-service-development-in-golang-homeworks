This is a combined task on how to send requests, receive responses, work with parameters, headers, and also write tests.


We have some kind of search service:
* SearchClient - a structure with the FindUsers method, which sends a request to an external system and returns the result, slightly transforming it. The code is written.
* SearchServer is a kind of external system. Directly searches for data in the `dataset.xml` file. In production, it would run as a separate web service. I wrote the code.
