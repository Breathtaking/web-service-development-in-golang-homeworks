package main

import (
	"encoding/json"
	"encoding/xml"
	"errors"
	"io"
	"net/http"
	"os"
	"reflect"
	"sort"
	"strconv"
	"strings"
	"time"
)

const (
	age  string = "Age"
	name string = "Name"
	id   string = "Id"
)

type Root struct {
	Version string `xml:"version,attr"`
	List    []Row  `xml:"row"`
}
type Row struct {
	ID        int    `xml:"id"`
	LastName  string `xml:"last_name"`
	FirstName string `xml:"first_name"`
	Age       int    `xml:"age"`
	About     string `xml:"about"`
	Gender    string `xml:"gender"`
}

var FileName string = "dataset.xml"

func sorting(users *[]User, orderBy int, orderField string) {
	switch orderBy {
	case 1:
		switch orderField {
		case "Age":
			sort.Slice(*users, func(i, j int) bool { return (*users)[i].Age < (*users)[j].Age })
		case "Id":
			sort.Slice(*users, func(i, j int) bool { return (*users)[i].ID < (*users)[j].ID })
		case "Name":
			sort.Slice(*users, func(i, j int) bool { return (*users)[i].Name < (*users)[j].Name })
		}
	case -1:
		switch orderField {
		case "Age":
			sort.Slice(*users, func(i, j int) bool { return (*users)[i].Age > (*users)[j].Age })
		case "Id":
			sort.Slice(*users, func(i, j int) bool { return (*users)[i].ID > (*users)[j].ID })
		case "Name":
			sort.Slice(*users, func(i, j int) bool { return (*users)[i].Name > (*users)[j].Name })
		}
	}
}
func getParams(r *http.Request) (SearchRequest, error) {
	var req SearchRequest
	var err error
	req.Limit, err = strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		panic(err)
	}
	req.Limit--
	req.Offset, err = strconv.Atoi(r.URL.Query().Get("offset"))
	if err != nil {
		panic(err)
	}
	req.Query = r.URL.Query().Get("query")
	req.OrderField = r.URL.Query().Get("order_field")
	switch req.OrderField {
	case id, age, name:
	case "":
		req.OrderField = name
	default:
		return req, errors.New(`{"error":"OrderField invalid"}`)
	}
	req.OrderBy, err = strconv.Atoi(r.URL.Query().Get("order_by"))
	if err != nil {
		panic(err)
	}
	if req.OrderBy < -1 || req.OrderBy > 1 {
		return req, errors.New(`{"error":"Incorrect parameter: orderBy"}`)
	}
	return req, nil
}
func selectAppropriateUsers(users []User, query string) []User {
	if query != "" {
		queryUsers := []User{}
		for _, us := range users {
			if strings.Contains(us.About, query) || strings.Contains(us.Name, query) {
				queryUsers = append(queryUsers, us)
			}
		}
		users = queryUsers
	}
	return users
}
func finalCorrection(users []User, usersBefore []User, req SearchRequest) []User {
	tmp := false
	if len(users) > req.Limit {
		users = users[:req.Limit]
	}
	if !reflect.DeepEqual(users[len(users)-1], usersBefore[len(usersBefore)-1]) {
		tmp = true
	}
	sorting(&users, req.OrderBy, req.OrderField)
	if tmp && len(users) == req.Limit {
		users = append(users, usersBefore[len(usersBefore)-1])
	}
	return users
}
func SearchServer(w http.ResponseWriter, r *http.Request) {
	var err error
	accessTokens := map[string]bool{"AshurovDavid": true}
	reqAccessToken := r.Header.Get("AccessToken")
	if _, ok := accessTokens[reqAccessToken]; !ok {
		w.WriteHeader(http.StatusUnauthorized)
		_, err = w.Write([]byte(`{"error":"Access denied"}`))
		if err != nil {
			panic(err)
		}
		return
	}
	req, err := getParams(r)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, err = w.Write([]byte(err.Error()))
		if err != nil {
			panic(err)
		}
		return
	}
	fileReader, err := os.Open(FileName)
	defer fileReader.Close()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, err = w.Write([]byte(`{"error":"Cannot find data file"`))
		if err != nil {
			panic(err)
		}
		return
	}
	xmlData, err := io.ReadAll(fileReader)
	if err != nil {
		panic(err)
	}
	rawUsers := new(Root)
	err = xml.Unmarshal(xmlData, rawUsers)
	if err != nil {
		panic(err)
	}
	users := []User{}
	for _, rawUser := range rawUsers.List {
		users = append(users, User{rawUser.ID,
			rawUser.FirstName + rawUser.LastName,
			rawUser.Age, rawUser.About, rawUser.Gender})
	}
	if len(users) > req.Offset {
		users = users[req.Offset:]
	} else {
		w.WriteHeader(http.StatusBadRequest)
		_, err = w.Write([]byte(`{"error":"Parameter Offset is too high, the database includes fewer users"}`))
		if err != nil {
			panic(err)
		}
		return
	}
	tmpusers := users
	users = selectAppropriateUsers(users, req.Query)
	if len(users) > 0 {
		users = finalCorrection(users, tmpusers, req)
		resultJSON, err := json.Marshal(users)
		if err != nil {
			panic(err)
		}
		_, err = w.Write(resultJSON)
		if err != nil {
			panic(err)
		}
	} else {
		users = append(users, User{-1, "", 0, "", ""})
		resultJSON, err := json.Marshal(users)
		if err != nil {
			panic(err)
		}
		_, err = w.Write(resultJSON)
		if err != nil {
			panic(err)
		}
	}
}
func TimeoutServer(w http.ResponseWriter, r *http.Request) {
	time.Sleep(time.Second)
}
func FakeServer(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write([]byte("bad json"))
	if err != nil {
		panic(err)
	}
}
func FakeServerWithBadRequest(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusBadRequest)
	_, err := w.Write([]byte("bad json"))
	if err != nil {
		panic(err)
	}
}
