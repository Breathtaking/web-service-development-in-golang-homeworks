package main

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

type TestErrorCase struct {
	reqParams
	err string
}
type TestSortingCase struct {
	reqParams
	ids []int
}
type TestAmountCase struct {
	reqParams
	amount int
}
type reqParams struct {
	limit      int
	offset     int
	query      string
	orderField string
	orderBy    int
}

func TestBadSearcherParams(t *testing.T) {
	cases := []TestErrorCase{
		{reqParams{-5, 5, "df", "Age", 1}, "limit must be > 0"},
		{reqParams{10, -2, "s", "Age", 1}, "offset must be > 0"},
		{reqParams{10, 5, "s", "Gender", 1}, "OrderFeld Gender invalid"},
		{reqParams{10, 5, "s", "Age", 5}, "unknown bad request error: Incorrect parameter: orderBy"},
		{reqParams{10, 35, "s", "Age", 0}, "unknown bad request error: Parameter Offset is too high, the database includes fewer users"},
	}
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	srv := SearchClient{"AshurovDavid", ts.URL}
	for _, c := range cases {
		req := SearchRequest{c.limit, c.offset, c.query, c.orderField, c.orderBy}
		resp, err := srv.FindUsers(req)
		expectedErr := errors.New(c.err)
		if resp != nil || err.Error() != expectedErr.Error() {
			t.Errorf("Expected error: %s, got error:%v", expectedErr, err)
		}
	}
}

func TestSorting(t *testing.T) {
	cases := []TestSortingCase{{reqParams{3, 0, "", "", 0}, []int{0, 1, 2}},
		{reqParams{3, 0, "", "Age", 1}, []int{1, 0, 2}},
		{reqParams{3, 0, "", "Age", -1}, []int{2, 0, 1}},
		{reqParams{3, 0, "", "Name", 1}, []int{0, 2, 1}},
		{reqParams{3, 0, "", "Name", -1}, []int{1, 2, 0}},
		{reqParams{3, 0, "", "Id", 1}, []int{0, 1, 2}},
		{reqParams{3, 0, "", "Id", -1}, []int{2, 1, 0}},
	}
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	srv := SearchClient{"AshurovDavid", ts.URL}
	for _, c := range cases {
		req := SearchRequest{c.limit, c.offset, c.query, c.orderField, c.orderBy}
		resp, err := srv.FindUsers(req)
		if err != nil {
			t.Errorf("Error: %v", err)
		}
		sortedIDs := []int{}
		for _, us := range resp.Users {
			sortedIDs = append(sortedIDs, us.ID)
		}
		if !reflect.DeepEqual(sortedIDs, c.ids) {
			t.Errorf("Expected consequence: %v\nRecieved consequence: %v", c.ids, sortedIDs)
		}
	}
}
func TestAmountOfUsers(t *testing.T) {
	cases := []TestAmountCase{{reqParams{100, 0, "", "", 1}, 25},
		{reqParams{10, 34, "", "", 1}, 1},
		{reqParams{10, 30, "", "", 1}, 5},
		{reqParams{1, 33, "Knapp", "", 1}, 0},
		{reqParams{100, 0, "AshurovDavid", "", 1}, 0},
		{reqParams{100, 0, "o", "", 1}, 25},
		{reqParams{100, 17, "o", "", 1}, 18},
		{reqParams{10, 17, "", "", 1}, 10},
	}
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	srv := SearchClient{"AshurovDavid", ts.URL}
	for _, c := range cases {
		req := SearchRequest{c.limit, c.offset, c.query, c.orderField, c.orderBy}
		resp, err := srv.FindUsers(req)
		if err != nil {
			t.Errorf("Error: %v", err)
		}
		if resp.Users[0].ID == -1 {
			resp.Users = []User{}
		}
		if len(resp.Users) != c.amount {
			t.Errorf("Expected %v users, got %v", c.amount, len(resp.Users))
		}
	}
}
func TestFullMatching(t *testing.T) {
	req := SearchRequest{3, 0, "BoydWolf", "Age", 0}
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	srv := SearchClient{"AshurovDavid", ts.URL}
	resp, err := srv.FindUsers(req)
	if err != nil {
		t.Errorf("Error: %v", err)
	}
	expected := []User{{0, "BoydWolf", 22, "Nulla cillum enim voluptate consequat laborum esse excepteur occaecat commodo nostrud excepteur ut cupidatat. Occaecat minim incididunt ut proident ad sint nostrud ad laborum sint pariatur. Ut nulla commodo dolore officia. Consequat anim eiusmod amet commodo eiusmod deserunt culpa. Ea sit dolore nostrud cillum proident nisi mollit est Lorem pariatur. Lorem aute officia deserunt dolor nisi aliqua consequat nulla nostrud ipsum irure id deserunt dolore. Minim reprehenderit nulla exercitation labore ipsum.\n", "male"}}
	if !reflect.DeepEqual(expected, resp.Users) {
		t.Errorf("Expected answer:\n%v\nRecieved answer:\n%v", expected, resp.Users)
	}
}
func TestAccess(t *testing.T) {
	req := SearchRequest{3, 0, "", "", 0}
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	srv := SearchClient{"OtherMan", ts.URL}
	resp, err := srv.FindUsers(req)
	expectedErr := errors.New("bad AccessToken")
	if resp != nil || err.Error() != expectedErr.Error() {
		t.Errorf("Expected failed authorization, got error:%v", err)
	}
}
func TestBadFile(t *testing.T) {
	FileName = "dataset."
	req := SearchRequest{10, 5, "g", "", 1}
	ts := httptest.NewServer(http.HandlerFunc(SearchServer))
	srv := SearchClient{"AshurovDavid", ts.URL}
	resp, err := srv.FindUsers(req)
	expectedErr := errors.New("SearchServer fatal error")
	if resp != nil || err.Error() != expectedErr.Error() {
		t.Errorf("Expected internal server error, got error:%v", err)
	}
}
func TestTimeout(t *testing.T) {
	req := SearchRequest{10, 5, "g", "", 1}
	ts := httptest.NewServer(http.HandlerFunc(TimeoutServer))
	srv := SearchClient{"AshurovDavid", ts.URL}
	resp, err := srv.FindUsers(req)
	expectedErr := errors.New("timeout for limit=11&offset=5&order_by=1&order_field=&query=g")
	if resp != nil || err.Error() != expectedErr.Error() {
		t.Errorf("Expected timeoiut error, got error:%v", err)
	}
}
func TestServerAbsence(t *testing.T) {
	req := SearchRequest{10, 5, "g", "", 1}
	srv := SearchClient{"AshurovDavid", ""}
	resp, err := srv.FindUsers(req)
	expectedErr := errors.New("unknown error Get \"?limit=11&offset=5&order_by=1&order_field=&query=g\": unsupported protocol scheme \"\"")
	if resp != nil || err.Error() != expectedErr.Error() {
		t.Errorf("Expected unknown error, got error:%v", err)
	}
}
func TestFakeServer(t *testing.T) {
	req := SearchRequest{10, 5, "g", "", 1}
	ts := httptest.NewServer(http.HandlerFunc(FakeServer))
	srv := SearchClient{"AshurovDavid", ts.URL}
	resp, err := srv.FindUsers(req)
	expectedErr := errors.New("cant unpack result json: invalid character 'b' looking for beginning of value")
	if resp != nil || err.Error() != expectedErr.Error() {
		t.Errorf("Expected json error, got error:%v", err)
	}
}
func TestFakeServerWithBadRequest(t *testing.T) {
	req := SearchRequest{10, 5, "g", "", 1}
	ts := httptest.NewServer(http.HandlerFunc(FakeServerWithBadRequest))
	srv := SearchClient{"AshurovDavid", ts.URL}
	resp, err := srv.FindUsers(req)
	expectedErr := errors.New("cant unpack error json: invalid character 'b' looking for beginning of value")
	if resp != nil || err.Error() != expectedErr.Error() {
		t.Errorf("Expected json error, got error:%v", err)
	}
}
