package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"redditclone/pkg/handlers"
	"redditclone/pkg/middleware"
	"redditclone/pkg/post"
	"redditclone/pkg/user"
)

func main() {
	UserHandler := handlers.UserHandler{
		UserRepo: user.NewUserMemoryRepo(),
	}
	PostHandler := handlers.PostHandler{
		PostRepo: post.NewPostMemoryRepo(),
	}

	endPoints := mux.NewRouter()
	endPoints.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./static/html/index.html")
	})
	endPoints.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))
	// отдали статику

	endPoints.HandleFunc("/api/login", UserHandler.Authorize).Methods("POST")
	endPoints.HandleFunc("/api/register", UserHandler.SignUp).Methods("POST")
	// привязали хэндлеры авторизации и регистрации

	endPoints.Handle("/api/posts", middleware.AuthMiddleware(http.HandlerFunc(PostHandler.AddPost))).Methods("POST")
	endPoints.Handle("/api/post/{postID:.{24}}", middleware.AuthMiddleware(http.HandlerFunc(PostHandler.AddComment))).Methods("POST")
	endPoints.Handle("/api/post/{postID:.{24}}", middleware.AuthMiddleware(http.HandlerFunc(PostHandler.DeletePost))).Methods("DELETE")
	endPoints.Handle("/api/post/{postID:.{24}}/{commentID:.{24}}", middleware.AuthMiddleware(http.HandlerFunc(PostHandler.DeleteComment))).Methods("DELETE")
	endPoints.Handle("/api/post/{postID:.{24}}/upvote", middleware.AuthMiddleware(http.HandlerFunc(PostHandler.Upvote))).Methods("GET")
	endPoints.Handle("/api/post/{postID:.{24}}/downvote", middleware.AuthMiddleware(http.HandlerFunc(PostHandler.Downvote))).Methods("GET")
	endPoints.Handle("/api/post/{postID:.{24}}/unvote", middleware.AuthMiddleware(http.HandlerFunc(PostHandler.Unvote))).Methods("GET")
	// привязали хэндлеры, требующие авторизации

	endPoints.HandleFunc("/api/user/{username:.+}", PostHandler.GetAllPostsOfUser).Methods("GET")
	endPoints.HandleFunc("/api/posts/", PostHandler.GetAllPosts).Methods("GET")
	endPoints.HandleFunc("/api/posts/{category:[a-z]+}", PostHandler.GetAllPostsOfCategory).Methods("GET")
	endPoints.HandleFunc("/api/post/{postID:.{24}}", PostHandler.ShowPostDetails).Methods("GET")
	// привязали хэндлеры, не требующие авторизации

	endPoints.Use(middleware.AccessLogMiddleware)
	endPoints.Use(middleware.PanicHandlingMiddleware)
	log.Fatal(http.ListenAndServe(":8082", endPoints))
}
