package servicefunctions

import (
	"math/rand"
	"time"
)

func CreateRandomStr() string {
	rand.Seed(time.Now().UnixNano())
	chars := "1234567890abcdefghigklmnopqrstuvwxyz"
	res := ""
	for i := 0; i < 24; i++ {
		res += string(chars[rand.Intn(36)])
	}
	return res
}
