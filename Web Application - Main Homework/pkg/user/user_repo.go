package user

import (
	"errors"
	"redditclone/pkg/servicefunctions"
	"sync"
)

type UserMemoryRepo struct {
	users map[string]*User
	mu    *sync.Mutex
}

var (
	ErrUserAlreadyExists = errors.New("user already exists")
	ErrUserNotExists     = errors.New("user doesn't exists")
	ErrIncorrectPassword = errors.New("incorrect password")
)

func NewUserMemoryRepo() *UserMemoryRepo {
	return &UserMemoryRepo{map[string]*User{}, &sync.Mutex{}}
}
func (repo *UserMemoryRepo) SignUp(login, pass string) (*User, error) {
	repo.mu.Lock()
	defer repo.mu.Unlock()
	if user, exist := repo.users[login]; exist {
		return user, ErrUserAlreadyExists
	}
	repo.users[login] = &User{
		ID:       servicefunctions.CreateRandomStr(),
		Login:    login,
		password: pass,
	}
	return repo.users[login], nil
}
func (repo *UserMemoryRepo) Authorize(login, pass string) (User, error) {
	repo.mu.Lock()
	user, exist := repo.users[login]
	tmp := *user
	repo.mu.Unlock()
	if !exist {
		return User{}, ErrUserNotExists
	}
	if tmp.password != pass {
		return tmp, ErrIncorrectPassword
	}
	return tmp, nil
}
