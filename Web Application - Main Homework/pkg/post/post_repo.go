package post

import (
	"errors"
	"redditclone/pkg/servicefunctions"
	"sort"
	"sync"
	"time"
)

type PostMemoryRepo struct {
	sync.Mutex
	categoryPosts map[string][]Post
	userPosts     map[string][]Post
}

var (
	ErrNoCategory = errors.New("there is no such category")
	ErrNoPost     = errors.New("there is no such post")
	ErrNoComment  = errors.New("there is no such comment")
)

func NewPostMemoryRepo() *PostMemoryRepo {
	return &PostMemoryRepo{
		categoryPosts: map[string][]Post{
			"music":       []Post{},
			"funny":       []Post{},
			"videos":      []Post{},
			"fashion":     []Post{},
			"news":        []Post{},
			"programming": []Post{},
		},
		userPosts: map[string][]Post{},
	}
}
func sortPostsByRate(posts []Post) []Post {
	sortedPosts := &posts
	sort.Slice(*sortedPosts, func(i, j int) bool { return (*sortedPosts)[i].Score > (*sortedPosts)[j].Score })
	return *sortedPosts
}
func sortPostsByDate(posts []Post) []Post {
	sortedPosts := &posts
	sort.Slice(*sortedPosts, func(i, j int) bool {
		return (*sortedPosts)[i].CreateUnixTime > (*sortedPosts)[j].CreateUnixTime
	})
	return *sortedPosts
}
func (r *PostMemoryRepo) GetAllPosts() []Post {
	allPosts := make([]Post, 0)
	r.Lock()
	for _, catPosts := range r.categoryPosts {
		allPosts = append(allPosts, catPosts...)
	}
	r.Unlock()
	allPosts = sortPostsByRate(allPosts)
	return allPosts
}
func (r *PostMemoryRepo) GetAllPostsOfUser(login string) ([]Post, error) {
	r.Lock()
	userPosts, exists := r.userPosts[login]
	if !exists {
		r.userPosts[login] = []Post{}
	}
	r.Unlock()
	userPosts = sortPostsByDate(userPosts)
	return userPosts, nil
}
func (r *PostMemoryRepo) AddPost(username, id, category, title, postType, text string) Post {
	newPost := Post{
		Author: struct {
			AuthorUsername string `json:"username"`
			AuthorID       string `json:"id"`
		}{username, id},
		Category:         category,
		Comments:         []Comment{},
		CreateDate:       time.Now().Format("2006-01-02T15:04:05.000") + "Z",
		CreateUnixTime:   time.Now().Unix(),
		ID:               servicefunctions.CreateRandomStr(),
		Score:            1,
		Type:             postType,
		Title:            title,
		UpvotePercentage: 100,
		Views:            0,
		Votes: []Vote{
			{id, 1},
		},
	}
	if postType == "text" {
		newPost.Text = text
	} else {
		newPost.URL = text
	}
	r.Lock()
	r.categoryPosts[category] = append(r.categoryPosts[category], newPost)
	r.userPosts[username] = append(r.userPosts[username], newPost)
	r.Unlock()
	return newPost
}
func (r *PostMemoryRepo) GetAllPostsOfCategory(category string) ([]Post, error) {
	r.Lock()
	categoryPosts, exists := r.categoryPosts[category]
	r.Unlock()
	if !exists {
		return nil, ErrNoCategory
	}
	categoryPosts = sortPostsByRate(categoryPosts)
	return categoryPosts, nil
}
func (r *PostMemoryRepo) ShowPostDetails(postID string) (Post, error) {
	var desiredPost *Post
	r.Lock()
Loop:
	for _, cat := range r.categoryPosts {
		for i, post := range cat {
			if post.ID == postID {
				desiredPost = &cat[i]
				break Loop
			}
		}
	}
	if desiredPost == nil {
		return Post{}, ErrNoPost
	}
	desiredPost.Views += 1
	var desiredPost2 *Post
Loop2:
	for _, u := range r.userPosts {
		for i, post := range u {
			if post.ID == postID {
				desiredPost2 = &u[i]
				break Loop2
			}
		}
	}
	*desiredPost2 = *desiredPost
	tmp := *desiredPost
	r.Unlock()
	return tmp, nil
}
func (r *PostMemoryRepo) AddComment(postID, commentBody, username, userID string) (Post, error) {
	newComment := Comment{
		Author: struct {
			AuthorUsername string `json:"username"`
			AuthorID       string `json:"id"`
		}{username, userID},
		ID:         servicefunctions.CreateRandomStr(),
		Body:       commentBody,
		CreateDate: time.Now().Format("2006-01-02T15:04:05.000") + "Z",
	}
	var desiredPost1 *Post
	category := ""
	r.Lock()
Loop:
	for _, u := range r.userPosts {
		for i, post := range u {
			if post.ID == postID {
				desiredPost1 = &u[i]
				break Loop
			}
		}
	}
	if desiredPost1 == nil {
		return Post{}, ErrNoPost
	}
	category = desiredPost1.Category
	desiredPost1.Comments = append(desiredPost1.Comments, newComment)
	var desiredPost2 *Post
	for i, post := range r.categoryPosts[category] {
		if post.ID == postID {
			desiredPost2 = &r.categoryPosts[category][i]
			break
		}
	}
	*desiredPost2 = *desiredPost1
	tmp := *desiredPost1
	r.Unlock()
	return tmp, nil
}
func (r *PostMemoryRepo) DeleteComment(postID, commentID string) (Post, error) {
	postIdx, commentIdx := -1, -1
	category := ""
	var desiredPost1 *Post
	r.Lock()
Loop:
	for _, u := range r.userPosts {
		for i, post := range u {
			if post.ID == postID {
				postIdx = i
				category = post.Category
				desiredPost1 = &u[i]
				for j, comment := range post.Comments {
					if comment.ID == commentID {
						commentIdx = j
						break Loop
					}
				}
			}
		}
	}
	if postIdx == -1 {
		return Post{}, ErrNoPost
	}
	if commentIdx == -1 {
		return Post{}, ErrNoComment
	}
	if len(desiredPost1.Comments) > 1 {
		copy(desiredPost1.Comments[commentIdx:], desiredPost1.Comments[commentIdx+1:])
		desiredPost1.Comments[len(desiredPost1.Comments)-1] = Comment{}
		desiredPost1.Comments = desiredPost1.Comments[:len(desiredPost1.Comments)-1]
	} else {
		desiredPost1.Comments = []Comment{}
	}
	var desiredPost2 *Post
	for i, post := range r.categoryPosts[category] {
		if post.ID == postID {
			desiredPost2 = &r.categoryPosts[category][i]
			break
		}
	}
	*desiredPost2 = *desiredPost1
	tmp := *desiredPost1
	r.Unlock()
	return tmp, nil
}
func (r *PostMemoryRepo) Vote(postID, userID string, voteValue int) (Post, error) {
	var desiredPost1 *Post
	category := ""
	r.Lock()
Loop:
	for _, u := range r.userPosts {
		for i, post := range u {
			if post.ID == postID {
				desiredPost1 = &u[i]
				break Loop
			}
		}
	}
	if desiredPost1 == nil {
		return Post{}, ErrNoPost
	}
	var existingVote *Vote
	voteIdx := -1
	for i, vote := range desiredPost1.Votes {
		if vote.UserID == userID {
			existingVote = &desiredPost1.Votes[i]
			voteIdx = i
			break
		}
	}
	if existingVote != nil {
		if existingVote.Vote == int8(voteValue) {
			tmp := *desiredPost1
			r.Unlock()
			return tmp, nil
		} else {
			if existingVote.Vote == 1 {
				desiredPost1.Score -= 1
			} else {
				desiredPost1.Score += 1
			}
			copy(desiredPost1.Votes[voteIdx:], desiredPost1.Votes[voteIdx+1:])
			desiredPost1.Votes[len(desiredPost1.Votes)-1] = Vote{}
			desiredPost1.Votes = desiredPost1.Votes[:len(desiredPost1.Votes)-1]
		}
	}
	newVote := Vote{
		UserID: userID,
	}
	if voteValue == 1 {
		desiredPost1.Score += 1
		newVote.Vote = 1
	} else {
		desiredPost1.Score -= 1
		newVote.Vote = -1
	}
	desiredPost1.Votes = append(desiredPost1.Votes, newVote)
	if len(desiredPost1.Votes) != 0 {
		positiveVotes := 0
		for _, vote := range desiredPost1.Votes {
			if vote.Vote == 1 {
				positiveVotes++
			}
		}
		desiredPost1.UpvotePercentage = 100 * positiveVotes / len(desiredPost1.Votes)
	} else {
		desiredPost1.UpvotePercentage = 0
	}
	category = desiredPost1.Category
	var desiredPost2 *Post
	for i, post := range r.categoryPosts[category] {
		if post.ID == postID {
			desiredPost2 = &r.categoryPosts[category][i]
		}
	}
	*desiredPost2 = *desiredPost1
	tmp := *desiredPost1
	r.Unlock()
	return tmp, nil
}
func (r *PostMemoryRepo) Unvote(postID, userID string) (Post, error) {
	var desiredPost1 *Post
	category := ""
	r.Lock()
Loop:
	for _, u := range r.userPosts {
		for i, post := range u {
			if post.ID == postID {
				desiredPost1 = &u[i]
				break Loop
			}
		}
	}
	if desiredPost1 == nil {
		return Post{}, ErrNoPost
	}
	category = desiredPost1.Category
	var voteIdx int
	var voteValue int8
	for i, vote := range desiredPost1.Votes {
		if vote.UserID == userID {
			voteIdx = i
			voteValue = vote.Vote
		}
	}
	if voteValue == 1 {
		desiredPost1.Score -= 1
	} else {
		desiredPost1.Score += 1
	}
	copy(desiredPost1.Votes[voteIdx:], desiredPost1.Votes[voteIdx+1:])
	desiredPost1.Votes[len(desiredPost1.Votes)-1] = Vote{}
	desiredPost1.Votes = desiredPost1.Votes[:len(desiredPost1.Votes)-1]
	if len(desiredPost1.Votes) != 0 {
		positiveVotes := 0
		for _, vote := range desiredPost1.Votes {
			if vote.Vote == 1 {
				positiveVotes++
			}
		}
		desiredPost1.UpvotePercentage = 100 * positiveVotes / len(desiredPost1.Votes)
	} else {
		desiredPost1.UpvotePercentage = 0
	}
	var desiredPost2 *Post
	for i, post := range r.categoryPosts[category] {
		if post.ID == postID {
			desiredPost2 = &r.categoryPosts[category][i]
			break
		}
	}
	*desiredPost2 = *desiredPost1
	tmp := *desiredPost1
	r.Unlock()
	return tmp, nil
}
func (r *PostMemoryRepo) DeletePost(postID, username string) (string, error) {
	var idx int
	category := ""
	r.Lock()
	for i, post := range r.userPosts[username] {
		if post.ID == postID {
			idx = i
			category = post.Category
			break
		}
	}
	if category == "" {
		return "", ErrNoPost
	}
	copy(r.userPosts[username][idx:], r.userPosts[username][idx+1:])
	r.userPosts[username][len(r.userPosts[username])-1] = Post{}
	r.userPosts[username] = r.userPosts[username][:len(r.userPosts[username])-1]
	for i, post := range r.categoryPosts[category] {
		if post.ID == postID {
			idx = i
			break
		}
	}
	copy(r.categoryPosts[category][idx:], r.categoryPosts[category][idx+1:])
	r.categoryPosts[category][len(r.categoryPosts[category])-1] = Post{}
	r.categoryPosts[category] = r.categoryPosts[category][:len(r.categoryPosts[category])-1]
	r.Unlock()
	return "{\"message\":\"success\"}", nil
}
