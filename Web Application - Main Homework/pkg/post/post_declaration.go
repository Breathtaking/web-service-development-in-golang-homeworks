package post

type Vote struct {
	UserID string `json:"user"`
	Vote   int8   `json:"vote"`
}
type Comment struct {
	CreateDate string `json:"created"`
	Author     struct {
		AuthorUsername string `json:"username"`
		AuthorID       string `json:"id"`
	} `json:"author"`
	Body string `json:"body"`
	ID   string `json:"id"`
}
type Post struct {
	Score  int    `json:"score"`
	Views  int    `json:"views"`
	Type   string `json:"type"`
	Title  string `json:"title"`
	URL    string `json:"url,omitempty"`
	Author struct {
		AuthorUsername string `json:"username"`
		AuthorID       string `json:"id"`
	} `json:"author"`
	Category         string    `json:"category"`
	Text             string    `json:"text,omitempty"`
	Votes            []Vote    `json:"votes"`
	Comments         []Comment `json:"comments"`
	CreateDate       string    `json:"created"`
	UpvotePercentage int       `json:"upvotePercentage"`
	ID               string    `json:"id"`
	CreateUnixTime   int64     `json:"-"`
}
type PostRepo interface {
	GetAllPosts() []Post
	GetAllPostsOfUser(login string) ([]Post, error)
	AddPost(username, id, category, title, postType, text string) Post
	GetAllPostsOfCategory(category string) ([]Post, error)
	ShowPostDetails(postID string) (Post, error)
	AddComment(postID, commentBody, username, userID string) (Post, error)
	DeleteComment(postID, commentID string) (Post, error)
	Vote(postID, userID string, voteValue int) (Post, error)
	Unvote(postID, userID string) (Post, error)
	DeletePost(postID, username string) (string, error)
}
