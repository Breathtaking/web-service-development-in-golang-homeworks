package handlers

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"io"
	"net/http"
	"redditclone/pkg/middleware"
	"redditclone/pkg/post"
)

type PostHandler struct {
	PostRepo post.PostRepo
}

func (h *PostHandler) GetAllPosts(w http.ResponseWriter, r *http.Request) {
	allPosts := h.PostRepo.GetAllPosts()
	var err error
	result, err := json.Marshal(allPosts)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	_, err = w.Write(result)
	if err != nil {
		w.WriteHeader(500)
	}
}

func (h *PostHandler) GetAllPostsOfUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	allPosts, err := h.PostRepo.GetAllPostsOfUser(vars["username"])
	if err != nil {
		w.WriteHeader(500)
		return
	}
	result, err := json.Marshal(allPosts)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	_, err = w.Write(result)
	if err != nil {
		w.WriteHeader(500)
	}
}
func (h *PostHandler) GetAllPostsOfCategory(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	allPosts, err := h.PostRepo.GetAllPostsOfCategory(vars["category"])
	if err != nil {
		w.WriteHeader(400)
		_, er := w.Write([]byte(err.Error()))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	result, err := json.Marshal(allPosts)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	_, err = w.Write(result)
	if err != nil {
		w.WriteHeader(500)
	}
}
func (h *PostHandler) AddPost(w http.ResponseWriter, r *http.Request) {
	userInfo := r.Context().Value("userInfo").(middleware.UserInfo)
	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, er := w.Write([]byte(err.Error()))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	err = r.Body.Close()
	if err != nil {
		w.WriteHeader(500)
		return
	}
	textData := struct {
		Category string `json:"category"`
		Text     string `json:"text"`
		Title    string `json:"title"`
		Type     string `json:"type"`
	}{}
	linkData := struct {
		Category string `json:"category"`
		URL      string `json:"url"`
		Title    string `json:"title"`
		Type     string `json:"type"`
	}{}
	err = json.Unmarshal(body, &textData)
	err2 := json.Unmarshal(body, &linkData)
	if err != nil && err2 != nil {
		w.WriteHeader(400)
		_, er := w.Write([]byte("cannot unmarshal JSON data"))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	if textData.Text == "" {
		textData.Text = linkData.URL
	}
	newPost := h.PostRepo.AddPost(userInfo.Username, userInfo.ID, textData.Category, textData.Title, textData.Type, textData.Text)
	result, err := json.Marshal(newPost)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	_, err = w.Write(result)
	if err != nil {
		w.WriteHeader(500)
	}
}
func (h *PostHandler) ShowPostDetails(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	desiredPost, err := h.PostRepo.ShowPostDetails(vars["postID"])
	if err != nil {
		w.WriteHeader(400)
		_, er := w.Write([]byte(err.Error()))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	result, err := json.Marshal(desiredPost)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	_, err = w.Write(result)
	if err != nil {
		w.WriteHeader(500)
	}
}
func (h *PostHandler) AddComment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userInfo := r.Context().Value("userInfo").(middleware.UserInfo)
	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, er := w.Write([]byte(err.Error()))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	err = r.Body.Close()
	if err != nil {
		w.WriteHeader(500)
		return
	}
	comment := struct {
		Comment string `json:"comment"`
	}{}
	err = json.Unmarshal(body, &comment)
	if err != nil {
		w.WriteHeader(400)
		_, er := w.Write([]byte("cannot unmarshal JSON data"))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	changedPost, err := h.PostRepo.AddComment(vars["postID"], comment.Comment, userInfo.Username, userInfo.ID)
	if err != nil {
		w.WriteHeader(400)
		_, er := w.Write([]byte("there is no such post"))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	result, err := json.Marshal(changedPost)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	_, err = w.Write(result)
	if err != nil {
		w.WriteHeader(500)
	}
}
func (h *PostHandler) DeleteComment(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	changedPost, err := h.PostRepo.DeleteComment(vars["postID"], vars["commentID"])
	if err != nil {
		w.WriteHeader(400)
		_, er := w.Write([]byte(err.Error()))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	result, err := json.Marshal(changedPost)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	_, err = w.Write(result)
	if err != nil {
		w.WriteHeader(500)
	}
}
func (h *PostHandler) Upvote(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userInfo := r.Context().Value("userInfo").(middleware.UserInfo)
	changedPost, err := h.PostRepo.Vote(vars["postID"], userInfo.ID, 1)
	if err != nil {
		w.WriteHeader(400)
		_, er := w.Write([]byte(err.Error()))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	result, err := json.Marshal(changedPost)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	_, err = w.Write(result)
	if err != nil {
		w.WriteHeader(500)
	}
}
func (h *PostHandler) Downvote(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userInfo := r.Context().Value("userInfo").(middleware.UserInfo)
	changedPost, err := h.PostRepo.Vote(vars["postID"], userInfo.ID, -1)
	if err != nil {
		w.WriteHeader(400)
		_, er := w.Write([]byte(err.Error()))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	result, err := json.Marshal(changedPost)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	_, err = w.Write(result)
	if err != nil {
		w.WriteHeader(500)
	}
}
func (h *PostHandler) Unvote(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userInfo := r.Context().Value("userInfo").(middleware.UserInfo)
	changedPost, err := h.PostRepo.Unvote(vars["postID"], userInfo.ID)
	if err != nil {
		w.WriteHeader(400)
		_, er := w.Write([]byte(err.Error()))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	result, err := json.Marshal(changedPost)
	if err != nil {
		w.WriteHeader(500)
		return
	}
	_, err = w.Write(result)
	if err != nil {
		w.WriteHeader(500)
	}
}
func (h *PostHandler) DeletePost(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	userInfo := r.Context().Value("userInfo").(middleware.UserInfo)
	str, err := h.PostRepo.DeletePost(vars["postID"], userInfo.Username)
	if err != nil {
		w.WriteHeader(400)
		_, er := w.Write([]byte(err.Error()))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	_, err = w.Write([]byte(str))
	if err != nil {
		w.WriteHeader(500)
	}
}
