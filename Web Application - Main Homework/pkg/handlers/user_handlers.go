package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"io"
	"net/http"
	"redditclone/pkg/user"
	"time"
)

type UserHandler struct {
	UserRepo user.UserRepo
}

var jwtKey string = "David's secret key"

func (h *UserHandler) SignUp(w http.ResponseWriter, r *http.Request) {
	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, er := w.Write([]byte(err.Error()))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	err = r.Body.Close()
	if err != nil {
		w.WriteHeader(500)
		return
	}
	data := &struct {
		Username string
		Password string
	}{}
	err = json.Unmarshal(body, data)
	if err != nil {
		w.WriteHeader(400)
		_, er := w.Write([]byte(err.Error()))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	newUser, err := h.UserRepo.SignUp(data.Username, data.Password)
	if err != nil {
		w.WriteHeader(http.StatusUnprocessableEntity)
		_, er := w.Write([]byte(fmt.Sprintf("{\"errors\":[{\"location\":\"body\",\"param\":\"username\",\"value\":\"%s\",\"msg\":\"already exists\"}]}", data.Username)))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user": map[string]string{
			"id":       newUser.ID,
			"username": newUser.Login,
		},
		"iat": time.Now().Unix(),
		"exp": time.Now().Add(time.Hour * 168).Unix(),
	})
	tokenString, err := token.SignedString([]byte(jwtKey))
	if err != nil {
		w.WriteHeader(500)
		return
	}
	_, err = w.Write([]byte(fmt.Sprintf("{\"token\":\"%s\"}", tokenString)))
	if err != nil {
		w.WriteHeader(500)
	}
}
func (h *UserHandler) Authorize(w http.ResponseWriter, r *http.Request) {
	body, err := io.ReadAll(r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		_, er := w.Write([]byte(err.Error()))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	err = r.Body.Close()
	if err != nil {
		w.WriteHeader(500)
		return
	}
	data := &struct {
		Username string
		Password string
	}{}
	err = json.Unmarshal(body, data)
	if err != nil {
		w.WriteHeader(400)
		_, er := w.Write([]byte(err.Error()))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	authUser, err := h.UserRepo.Authorize(data.Username, data.Password)
	if err != nil {
		w.WriteHeader(401)
		_, er := w.Write([]byte(fmt.Sprintf("{\"message\": \"%s\"}", err.Error())))
		if er != nil {
			w.WriteHeader(500)
		}
		return
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"user": map[string]string{
			"id":       authUser.ID,
			"username": authUser.Login,
		},
		"iat": time.Now().Unix(),
		"exp": time.Now().Add(time.Hour * 168).Unix(),
	})
	tokenString, err := token.SignedString([]byte(jwtKey))
	if err != nil {
		w.WriteHeader(500)
		return
	}
	_, err = w.Write([]byte(fmt.Sprintf("{\"token\":\"%s\"}", tokenString)))
	if err != nil {
		w.WriteHeader(500)
	}
}
