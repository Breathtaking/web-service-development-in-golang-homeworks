package middleware

import (
	"log"
	"net/http"
	"time"
)

func AccessLogMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()
		next.ServeHTTP(w, r)
		log.Printf("Method - %s,Remote address - %s,Request path - %s,Processing time - %s\n\n",
			r.Method, r.RemoteAddr, r.URL.Path, time.Since(start))
	})
}
