package middleware

import (
	"context"
	"errors"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"strings"
)

var (
	jwtKey              = "David's secret key"
	errBadSigningMethod = errors.New("bad signing method")
)

type UserInfo struct {
	Username string
	ID       string
}

func AuthMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header["Authorization"] != nil {
			inToken := r.Header.Get("authorization")
			inToken = strings.TrimPrefix(inToken, "Bearer ")
			keyFunc := func(token *jwt.Token) (interface{}, error) {
				method, ok := token.Method.(*jwt.SigningMethodHMAC)
				if !ok || method.Alg() != "HS256" {
					return nil, errBadSigningMethod
				}
				return []byte(jwtKey), nil
			}
			token, err := jwt.Parse(inToken, keyFunc)
			if err != nil || !token.Valid {
				w.WriteHeader(401)
				_, er := w.Write([]byte("bad token"))
				if er != nil {
					w.WriteHeader(500)
				}
				return
			}
			payload, ok := token.Claims.(jwt.MapClaims)
			if !ok {
				w.WriteHeader(401)
				_, er := w.Write([]byte("no payload"))
				if er != nil {
					w.WriteHeader(500)
				}
				return
			}
			userInfoMap := payload["user"].(map[string]interface{})
			userInfo := UserInfo{
				userInfoMap["username"].(string),
				userInfoMap["id"].(string),
			}
			ctx := r.Context()
			ctx = context.WithValue(ctx, "userInfo", userInfo)
			next.ServeHTTP(w, r.WithContext(ctx))
		} else {
			w.WriteHeader(401)
			_, err := w.Write([]byte("not authorized"))
			if err != nil {
				w.WriteHeader(500)
			}
		}
	})
}
