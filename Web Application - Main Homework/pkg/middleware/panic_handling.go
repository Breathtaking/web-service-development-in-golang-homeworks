package middleware

import (
	"fmt"
	"net/http"
)

func PanicHandlingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				w.WriteHeader(500)
				fmt.Println("panic recovered: ", err)
			}
		}()
		next.ServeHTTP(w, r)
	})
}
