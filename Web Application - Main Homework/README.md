Backend for Reddit clone.

There is a Reddit clone:
* https://asperitas.now.sh/ - web-version

Frontend there works entirely on JS and only requires me to give the necessary static: html, which will run all the JS, and JS will make AJAX requests to the backend.

All the backend implemented by me. All APIs return data in JSON format.

There are next API-methods:
1) POST /api/register - sign up
2) POST /api/login - login
3) GET /api/posts/ - list of all posts
4) POST /api/posts/ - add post
5) GET /api/funny/{CATEGORY_NAME} - list of posts in a specific category
6) GET /api/post/{POST_ID} - post details with comments
7) POST /api/post/{POST_ID} - add comment
8) DELETE /api/post/{POST_ID}/{COMMENT_ID} - delete comment
9) GET /api/post/{POST_ID}/upvote - upvote the post
10) GET /api/post/{POST_ID}/downvote - downvote the post
11) GET /api/post/{POST_ID}/unvote - delete vote from the post
12) DELETE /api/post/{POST_ID} - delete post
13) GET /api/user/{USER_LOGIN} - list of all posts of user

Requirements:
* Clean architecture which means distribution of code in different folders, i.e. folder for middleware, folder for HTTP handlers.
* Repository pattern for data access
* Using JWT-tokens for authorization
* Access log









