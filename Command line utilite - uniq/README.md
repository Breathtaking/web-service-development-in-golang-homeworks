Here is the utility that can display or filter out duplicate lines in a file (the equivalent of the UNIX uniq utility). And repeated input strings should not be recognized if they do not strictly follow each other. The utility itself has a set of parameters that must be supported.

Parameters:
-с - count the number of appearances of a string in the input. Output this number before the string separated by a space.

-d - output only duplicate strings.

-u - output only unique strings.

-f num_fields - don't consider first num_fields fields of string. A field in a string is a non-empty set of characters separated by a space.

-s num_chars - don't consider first num_chars chars of string. When used with the -f option, don't consider the first characters after num_fields fields (ignoring the delimiter space after the last field).

-i - don't consider register of letters.

Using:

uniq [-c | -d | -u] [-i] [-f num] [-s chars] [input_file [output_file]]

All parameters are optional. The behavior of the utility without parameters is simply outputting unique strings from the input.

Parameters c, d, u are interchangeable. It must be considered that using these parameters together doesn't make any sense. Passing one along with the other should display to the user the correct usage of the utility.

If input_file is not passed, then consider stdin as the input stream

If output_file is not passed, then consider stdout as the output stream
