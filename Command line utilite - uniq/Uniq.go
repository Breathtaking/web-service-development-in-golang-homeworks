package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"strings"
	"unicode/utf8"
)

type Options struct {
	c, d, u, i            bool
	f, s                  int
	inputFile, outputFile string
}
type occurences struct {
	position, amount int
}

var options Options

func (o *Options) Init() {
	optionC := flag.Bool("c", false, "Подсчитывает количество появлений строки и выводит перед строкой")
	optionD := flag.Bool("d", false, "Выводит только те строки,которые повторились во входных данных")
	optionU := flag.Bool("u", false, "Выводит только те строки,которые не повторились во входных данных")
	optionF := flag.Int("f", 0, "Не учитывает первые value полей в строке")
	optionS := flag.Int("s", 0, "Не учитывает первые value символов в строке")
	optionI := flag.Bool("i", false, "Не учитывает регистр букв")
	flag.Parse()
	o.c = *optionC
	o.d = *optionD
	o.u = *optionU
	if o.c && o.d || o.c && o.u || o.d && o.u {
		panic("Параметры -c , -d и -u взаимозаменяемы. Использование одного из них исключает использование других.")
	}
	o.i = *optionI
	o.f = *optionF
	o.s = *optionS
	o.inputFile = flag.Arg(0)
	o.outputFile = flag.Arg(1)
}
func useOptionF(mas []string, num int) []string {
	var newMas []string
	for _, str := range mas {
		str = strings.TrimSpace(str)
		for i := 0; i < num; i++ {
			pos := strings.Index(str, " ")
			if pos > 0 {
				_, after, _ := strings.Cut(str, " ")
				str = after
			} else if pos == 0 {
				_, after, _ := strings.Cut(str, " ")
				str = after
				i--
			} else {
				str = ""
				break
			}
		}
		newMas = append(newMas, str)
	}
	return newMas
}
func useOptionS(mas []string, num int) []string {
	var newMas []string
	for _, str := range mas {
		if utf8.RuneCountInString(str) > num {
			runes := []rune(str)
			runes = runes[num:]
			str = string(runes)
		} else {
			str = ""
		}
		newMas = append(newMas, str)
	}
	return newMas
}
func useOptionI(mas []string) []string {
	for i := range mas {
		mas[i] = strings.ToLower(mas[i])
	}
	return mas
}
func useOptionD(mas []string, occs []occurences) []string {
	var newMas []string
	for _, record := range occs {
		if record.amount > 1 {
			newMas = append(newMas, mas[record.position])
		}
	}
	return newMas
}
func useOptionU(mas []string, occs []occurences) []string {
	var newMas []string
	for _, record := range occs {
		if record.amount == 1 {
			newMas = append(newMas, mas[record.position])
		}
	}
	return newMas
}
func useOptionC(mas []string, occs []occurences) []string {
	var newMas []string
	for _, record := range occs {
		str := fmt.Sprintf("%v %v", record.amount, mas[record.position])
		newMas = append(newMas, str)
	}
	return newMas
}
func uniq(mas []string) []string {
	tempMas := make([]string, len(mas))
	copy(tempMas, mas)
	if options.f > 0 {
		tempMas = useOptionF(tempMas, options.f)
	}
	if options.s > 0 {
		tempMas = useOptionS(tempMas, options.s)
	}
	if options.i {
		tempMas = useOptionI(tempMas)
	}
	var occs []occurences
	occsIterator := 0
	for i := range tempMas {
		switch {
		case i == 0:
			occs = append(occs, occurences{i, 1})
		case tempMas[i] == tempMas[i-1]:
			occs[occsIterator].amount++
		case tempMas[i] != tempMas[i-1]:
			occs = append(occs, occurences{i, 1})
			occsIterator++
		}
	}
	var newMas []string
	switch {
	case options.c:
		newMas = useOptionC(mas, occs)
	case options.d:
		newMas = useOptionD(mas, occs)
	case options.u:
		newMas = useOptionU(mas, occs)
	default:
		for _, record := range occs {
			newMas = append(newMas, mas[record.position])
		}
	}
	return newMas
}

func main() {
	options.Init()
	var scanner *bufio.Scanner
	if options.inputFile == "" {
		fmt.Println("Вводите строки,для завершения ввода введите Ctrl+Z.")
		scanner = bufio.NewScanner(os.Stdin)
	} else {
		readFile, err := os.Open(options.inputFile)
		defer readFile.Close()
		if err != nil {
			fmt.Fprintln(os.Stderr, "error: ", err)
			os.Exit(1)
		}
		scanner = bufio.NewScanner(readFile)
	}
	var textLines []string
	for scanner.Scan() {
		textLines = append(textLines, scanner.Text())
	}
	if err := scanner.Err(); err != nil {
		fmt.Fprintln(os.Stderr, "error: ", err)
		os.Exit(1)
	}
	//Получен слайс строк из входного потока данных
	var writer *bufio.Writer
	if options.outputFile == "" {
		writer = bufio.NewWriter(os.Stdout)
	} else {
		newFile, err := os.Create(options.outputFile)
		if err != nil {
			fmt.Fprintln(os.Stderr, "error: ", err)
			os.Exit(1)
		}
		writer = bufio.NewWriter(newFile)
		defer newFile.Close()
	}
	writer.WriteString("Введенные строки:\n")
	for _, str := range textLines {
		str = str + "\n"
		writer.WriteString(str)
	}
	writer.WriteString("------------------------\n")
	writer.WriteString("Результат:\n")
	textLines = uniq(textLines)
	for _, str := range textLines {
		str = str + "\n"
		writer.WriteString(str)
	}
	writer.Flush()
}
