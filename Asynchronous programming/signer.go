package main

import (
	"sort"
	"strconv"
	"strings"
	"sync"
)

var mu = &sync.Mutex{}

func Md5(data string) string {
	mu.Lock()
	data = DataSignerMd5(data)
	mu.Unlock()
	return data
}
func SingleHash(in, out chan interface{}) {
	highLvlWg := &sync.WaitGroup{}
	for num := range in {
		highLvlWg.Add(1)
		go func(n int) {
			var str1, str2 string
			lowLvlWg := &sync.WaitGroup{}
			lowLvlWg.Add(2)
			go func() {
				str1 = DataSignerCrc32(strconv.Itoa(n))
				// fmt.Printf("%v SingleHash crc32(data) %v\n", n, str1)
				lowLvlWg.Done()
			}()
			go func() {
				tmp := Md5(strconv.Itoa(n))
				// fmt.Printf("%v SingleHash md5(data) %v\n", n, tmp)
				str2 = DataSignerCrc32(tmp)
				// fmt.Printf("%v SingleHash crc32(md5(data)) %v\n", n, str2)
				lowLvlWg.Done()
			}()
			lowLvlWg.Wait()
			str3 := str1 + "~" + str2
			// fmt.Printf("%v SingleHash result %v\n", n, str3)
			out <- str3
			highLvlWg.Done()
		}(num.(int))
	}
	highLvlWg.Wait()
}
func MultiHash(in, out chan interface{}) {
	highLvlWg := &sync.WaitGroup{}
	for singleHashStr := range in {
		highLvlWg.Add(1)
		go func(sinHashStr string) {
			var crcStrings [6]string
			lowLvlWg := &sync.WaitGroup{}
			for iter := 0; iter <= 5; iter++ {
				lowLvlWg.Add(1)
				go func(i int) {
					crcStrings[i] = DataSignerCrc32(strconv.Itoa(i) + sinHashStr)
					// fmt.Printf("%v MultiHash crc32(th) %v %v\n", sinHashStr, i, crcStrings[i])
					lowLvlWg.Done()
				}(iter)
			}
			lowLvlWg.Wait()
			str := crcStrings[0] + crcStrings[1] + crcStrings[2] + crcStrings[3] + crcStrings[4] + crcStrings[5]
			// fmt.Printf("%v MultiHash result %v\n", sinHashStr, str)
			out <- str
			highLvlWg.Done()
		}(singleHashStr.(string))
	}
	highLvlWg.Wait()
}
func CombineResults(in, out chan interface{}) {
	multiHashStrings := []string{}
	for str := range in {
		multiHashStrings = append(multiHashStrings, str.(string))
	}
	sort.Strings(multiHashStrings)
	resultStr := ""
	for _, str := range multiHashStrings {
		resultStr += str + "_"
	}
	resultStr = strings.TrimSuffix(resultStr, "_")
	// fmt.Printf("Combine %v\n", resultStr)
	out <- resultStr
}
func ExecutePipeline(funcs ...job) {
	tmp := make(chan interface{})
	wg := &sync.WaitGroup{}
	for _, function := range funcs {
		in := tmp
		out := make(chan interface{})
		wg.Add(1)
		go func(f job) {
			f(in, out)
			close(out)
			wg.Done()
		}(function)
		tmp = out
	}
	wg.Wait()
}
func main() {
}
