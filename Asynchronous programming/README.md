The task consists of two parts:
* Writing an ExecutePipeline function that provides us with pipeline processing of worker functions that do something.
* Writing several functions that compute us hash sum from the input data.

The calculation of the hash sum is implemented by the following chain:

* SingleHash computes the value of crc32(data)+"~"+crc32(md5(data)) (concatenation of two strings through ~), where data is the input (in fact, the numbers from the first function)
* MultiHash computess the crc32(th+data)) value (the concatenation of the digit cast to the string and the string), where th=0..5 ( i.e. 6 hashes for each input value ), then takes the concatenation of the results in the order of calculation ( 0..5), where data is what came to the input (and went to the output from SingleHash)
* CombineResults gets all results, sorts it, combines sorted result with _ (underscore character) into one string
* crc32 is computing through the DataSignerCrc32 function
* md5 is computing through DataSignerMd5 function
