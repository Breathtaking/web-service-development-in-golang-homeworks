package main

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	tgbotapi "github.com/skinass/telegram-bot-api/v5"
	"html/template"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

var (
	// @BotFather в телеграме даст вам это
	BotToken = "5665326787:AAH8TbsnM7JMyi5JVvE4rQru4PjY7apMn8w"

	// урл выдаст вам игрок или хероку
	WebhookURL = "https://2d54-2a0e-9cc0-24a8-6800-3923-908f-4166-cb92.eu.ngrok.io"
)

type Task struct {
	ID               int
	Description      string
	Owner            int64
	OwnerUsername    string
	Assignee         int64
	AssigneeUsername string
	IsAssigned       bool
}
type command struct {
	regexp         string
	handleFunction func(user tgbotapi.User, comm string) ([]Answer, error)
}
type Answer struct {
	to      int64
	content bytes.Buffer
}

const (
	incorrectCommand        = `Команда введена некорректно`
	newTaskTmpl             = `Задача "{{.Description}}" создана, id={{.ID}}`
	assignTmpl              = `Задача "{{.Description}}" назначена на вас`
	reassignTmpl            = `Задача "{{.Description}}" назначена на @{{.AssigneeUsername}}`
	unassignFail            = `Задача не на вас`
	unassignSuccess         = `Принято`
	unassignNotification    = `Задача "{{.Description}}" осталась без исполнителя`
	resolveTmpl             = `Задача "{{.Description}}" выполнена`
	resolveNotificationTmpl = `Задача "{{.Description}}" выполнена @{{.AssigneeUsername}}`
	showNoTasksTmpl         = `Нет задач`
	showTasksTmpl           = `{{range .TmplTasks}}{{.ID}}. {{.Description}} by @{{.OwnerUsername}}
{{if not .IsAssigned}}/assign_{{.ID}}{{else if ne .Assignee $.SentFrom}}assignee: @{{.AssigneeUsername}}{{else}}assignee: я
/unassign_{{.ID}} /resolve_{{.ID}}{{end}}

{{end}}`
	showMyTasksTmpl = `{{range .TmplTasks}}{{if eq .Assignee $.SentFrom}}{{.ID}}. {{.Description}} by @{{.OwnerUsername}}
/unassign_{{.ID}} /resolve_{{.ID}}

{{end}}{{end}}`
	showMyOwnTasksTmpl = `{{range .TmplTasks}}{{if eq .Owner $.SentFrom}}{{.ID}}. {{.Description}} by @{{.OwnerUsername}}
/assign_{{.ID}}

{{end}}{{end}}`
)

var nextTaskID int = 1
var tasks []Task

func getTaskIdxByID(id int) int {
	for i, task := range tasks {
		if task.ID == id {
			return i
		}
	}
	return -1
}
func showTasks(user tgbotapi.User, comm string) ([]Answer, error) {
	var buf bytes.Buffer
	tmpl := template.New("showTasks")
	if len(tasks) == 0 {
		tmpl2 := template.New("showNoTasks")
		tmpl2, err := tmpl2.Parse(showNoTasksTmpl)
		if err != nil {
			return nil, err
		}
		err = tmpl2.Execute(&buf, nil)
		if err != nil {
			return nil, err
		}
		return []Answer{{user.ID, buf}}, nil
	}
	tmplStruct := struct {
		TmplTasks []Task
		SentFrom  int64
	}{tasks, user.ID}
	tmpl, err := tmpl.Parse(showTasksTmpl)
	if err != nil {
		return nil, err
	}
	err = tmpl.Execute(&buf, tmplStruct)
	if err != nil {
		return nil, err
	}
	buf.Truncate(buf.Len() - 2)
	return []Answer{{user.ID, buf}}, nil
}
func newTask(user tgbotapi.User, comm string) ([]Answer, error) {
	comm = strings.TrimPrefix(comm, "/new ")
	task := Task{nextTaskID, comm, user.ID, user.UserName, 0, "", false}
	tasks = append(tasks, task)
	nextTaskID++
	var buf bytes.Buffer
	tmpl := template.New("newTask")
	tmpl, err := tmpl.Parse(newTaskTmpl)
	if err != nil {
		return nil, err
	}
	err = tmpl.Execute(&buf, task)
	if err != nil {
		return nil, err
	}
	res := []Answer{
		{user.ID, buf},
	}
	return res, nil
}
func assign(user tgbotapi.User, comm string) ([]Answer, error) {
	taskID, err := strconv.Atoi(strings.TrimPrefix(comm, "/assign_"))
	if err != nil {
		return nil, err
	}
	taskIdx := getTaskIdxByID(taskID)
	if taskIdx == -1 {
		return nil, errors.New("такой задачи нет")
	}
	res := make([]Answer, 0)
	buf := bytes.Buffer{}
	tmpl := template.New("assign")
	tmpl, err = tmpl.Parse(assignTmpl)
	if err != nil {
		return nil, err
	}
	err = tmpl.Execute(&buf, tasks[taskIdx])
	if err != nil {
		return nil, err
	}
	res = append(res, Answer{user.ID, buf})
	tasks[taskIdx].AssigneeUsername = user.UserName
	if (tasks[taskIdx].IsAssigned && tasks[taskIdx].Assignee != user.ID) || (!tasks[taskIdx].IsAssigned && user.ID != tasks[taskIdx].Owner) {
		var user2 int64
		if tasks[taskIdx].IsAssigned && tasks[taskIdx].Assignee != user.ID {
			user2 = tasks[taskIdx].Assignee
		} else {
			user2 = tasks[taskIdx].Owner
		}
		buf2 := bytes.Buffer{}
		tmpl2 := template.New("reassign")
		tmpl2, err = tmpl2.Parse(reassignTmpl)
		if err != nil {
			return nil, err
		}
		err = tmpl2.Execute(&buf2, tasks[taskIdx])
		if err != nil {
			return nil, err
		}
		res = append(res, Answer{user2, buf2})
	}
	tasks[taskIdx].IsAssigned = true
	tasks[taskIdx].Assignee = user.ID
	return res, nil
}
func unassign(user tgbotapi.User, comm string) ([]Answer, error) {
	taskID, err := strconv.Atoi(strings.TrimPrefix(comm, "/unassign_"))
	if err != nil {
		return nil, err
	}
	taskIdx := getTaskIdxByID(taskID)
	if taskIdx == -1 {
		return nil, errors.New("такой задачи нет")
	}
	buf := bytes.Buffer{}
	tmpl := template.New("unassign")
	if tasks[taskIdx].Assignee != user.ID {
		tmpl, err = tmpl.Parse(unassignFail)
		if err != nil {
			return nil, err
		}
		err = tmpl.Execute(&buf, nil)
		if err != nil {
			return nil, err
		}
		return []Answer{{user.ID, buf}}, nil
	} else {
		tmpl, err = tmpl.Parse(unassignSuccess)
		if err != nil {
			return nil, err
		}
		err = tmpl.Execute(&buf, nil)
		if err != nil {
			return nil, err
		}
		buf2 := bytes.Buffer{}
		tmpl2 := template.New("unassignNotification")
		tmpl2, err = tmpl2.Parse(unassignNotification)
		if err != nil {
			return nil, err
		}
		err = tmpl2.Execute(&buf2, tasks[taskIdx])
		if err != nil {
			return nil, err
		}
		tasks[taskIdx].IsAssigned = false
		tasks[taskIdx].Assignee = 0
		tasks[taskIdx].AssigneeUsername = ""
		return []Answer{
			{user.ID, buf},
			{tasks[taskIdx].Owner, buf2},
		}, nil
	}
}
func resolve(user tgbotapi.User, comm string) ([]Answer, error) {
	taskID, err := strconv.Atoi(strings.TrimPrefix(comm, "/resolve_"))
	if err != nil {
		return nil, err
	}
	taskIdx := getTaskIdxByID(taskID)
	if taskIdx == -1 {
		return nil, errors.New("такой задачи нет")
	}
	buf := bytes.Buffer{}
	tmpl := template.New("resolve")
	res := make([]Answer, 0)
	if tasks[taskIdx].Assignee != user.ID {
		tmpl, err = tmpl.Parse(unassignFail)
		if err != nil {
			return nil, err
		}
		err = tmpl.Execute(&buf, nil)
		if err != nil {
			return nil, err
		}
		res = append(res, Answer{user.ID, buf})
	} else {
		tmpl, err = tmpl.Parse(resolveTmpl)
		if err != nil {
			return nil, err
		}
		err = tmpl.Execute(&buf, tasks[taskIdx])
		if err != nil {
			return nil, err
		}
		buf2 := bytes.Buffer{}
		tmpl2 := template.New("resolveNotification")
		tmpl2, err = tmpl2.Parse(resolveNotificationTmpl)
		if err != nil {
			return nil, err
		}
		err = tmpl2.Execute(&buf2, tasks[taskIdx])
		if err != nil {
			return nil, err
		}
		res = append(res, Answer{user.ID, buf})
		res = append(res, Answer{tasks[taskIdx].Owner, buf2})
		copy(tasks[taskIdx:], tasks[taskIdx+1:])
		tasks[len(tasks)-1] = Task{}
		tasks = tasks[:len(tasks)-1]
	}
	return res, nil
}
func showMyTasks(user tgbotapi.User, comm string) ([]Answer, error) {
	var buf bytes.Buffer
	tmpl := template.New("showMyTasks")
	tmpl2 := template.New("showNoTasks")
	tmplStruct := struct {
		TmplTasks []Task
		SentFrom  int64
	}{tasks, user.ID}
	tmpl, err := tmpl.Parse(showMyTasksTmpl)
	if err != nil {
		return nil, err
	}
	err = tmpl.Execute(&buf, tmplStruct)
	if err != nil {
		return nil, err
	}
	if buf.Len() != 0 {
		buf.Truncate(buf.Len() - 2)
	} else {
		tmpl2, err = tmpl2.Parse(showNoTasksTmpl)
		if err != nil {
			return nil, err
		}
		err = tmpl2.Execute(&buf, nil)
		if err != nil {
			return nil, err
		}
	}
	return []Answer{{user.ID, buf}}, nil
}
func showMyOwnTasks(user tgbotapi.User, comm string) ([]Answer, error) {
	var buf bytes.Buffer
	tmpl := template.New("showMyOwnTasks")
	tmpl2 := template.New("showNoTasks")
	tmplStruct := struct {
		TmplTasks []Task
		SentFrom  int64
	}{tasks, user.ID}
	tmpl, err := tmpl.Parse(showMyOwnTasksTmpl)
	if err != nil {
		return nil, err
	}
	err = tmpl.Execute(&buf, tmplStruct)
	if err != nil {
		return nil, err
	}
	if buf.Len() != 0 {
		buf.Truncate(buf.Len() - 2)
	} else {
		tmpl2, err = tmpl2.Parse(showNoTasksTmpl)
		if err != nil {
			return nil, err
		}
		err = tmpl2.Execute(&buf, nil)
		if err != nil {
			return nil, err
		}
	}
	return []Answer{{user.ID, buf}}, nil
}

func startTaskBot(_ context.Context) error {
	bot, err := tgbotapi.NewBotAPI(BotToken)
	if err != nil {
		fmt.Printf("NewBotAPI failed: %s", err)
		return err
	}
	bot.Debug = true
	fmt.Printf("Authorized on account %s\n", bot.Self.UserName)
	wh, err := tgbotapi.NewWebhook(WebhookURL)
	if err != nil {
		fmt.Printf("NewWebhook failed: %s", err)
		return err
	}
	_, err = bot.Request(wh)
	if err != nil {
		fmt.Printf("Set Webhook failed: %s", err)
		return err
	}
	updates := bot.ListenForWebhook("/")
	go func() {
		log.Fatalln("http err: ", http.ListenAndServe(":8081", nil))
	}()
	commands := []command{
		{`^/tasks$`, showTasks},
		{`^/new .+$`, newTask},
		{`^/assign_[\d]+$`, assign},
		{`^/unassign_[\d]+$`, unassign},
		{`^/resolve_[\d]+$`, resolve},
		{`^/my$`, showMyTasks},
		{`^/owner$`, showMyOwnTasks},
	}
	for update := range updates {
		answers := make([]Answer, 0)
		for _, comm := range commands {
			if match, er := regexp.MatchString(comm.regexp, update.Message.Text); match {
				if er != nil {
					fmt.Printf("RegExp matching check failed: %s", er)
					return er
				}
				answers, err = comm.handleFunction(*update.SentFrom(), update.Message.Text)
				break
			}
		}
		if err != nil {
			fmt.Printf("%s failed: %s", update.Message.Text, err)
			return err
		}
		if len(answers) == 0 {
			var buf bytes.Buffer
			tmpl := template.New("IncorrectCommand")
			tmpl, err = tmpl.Parse(incorrectCommand)
			if err != nil {
				fmt.Println(err)
				return err
			}
			err = tmpl.Execute(&buf, nil)
			if err != nil {
				fmt.Println(err)
				return err
			}
			answers = append(answers, Answer{update.SentFrom().ID, buf})
		}
		for _, ans := range answers {
			_, err = bot.Send(tgbotapi.NewMessage(ans.to, ans.content.String()))
			if err != nil {
				fmt.Printf("Send message failed: %s", err)
				return err
			}
		}
	}
	return nil
}

func main() {
	// just comment
	tasks = make([]Task, 0)
	err := startTaskBot(context.Background())
	if err != nil {
		panic(err)
	}
}
