In this task, we will implement a simple task management telegram bot. A bit of parsing, a bit of data modeling, hosting telegram bots via heroku. Responses are formed using templates.

Management through a text interface:

* `/tasks` - shows all tasks
* `/new XXX YYY ZZZ` - creates a new task
* `/assign_$ID` - makes the user a task executor
* `/unassign_$ID` - removes the task from the current executor
* `/resolve_$ID` - executes the task, removes it from the list
* `/my` - shows tasks that are assigned to me
* `/owner` - shows tasks created by me
